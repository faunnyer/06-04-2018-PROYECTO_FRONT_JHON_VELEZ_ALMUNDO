proyecto frontHotelAlmundo


# IMPORTANTE ¡¡¡

Para correr el siguiente proyecto correr los siguientes comandos

# npm install bootstrap@4.0.0-alpha.6
# npm install jquery
# npm install


# El api esta montado en un servidor 165.227.92.20 , solo es necesario descargar el proyecto front y correrlo  con npm start o ng serve

#EXTRAS DE LA APLICACION

* SE MONTA UNA API APUNTANDO A BASE DE DATOS MONGO CON UN CRUD DE HOTELES Y COMODIDADES INCORPORADO
* SE REALIZAN VALIDACIONES EXTRAS DE ACUERDO AL CRUD QUE SE INCORPORO (CAMOS OBLIGATORIOS, EXTENSIONES DE IMAGENES)

# A nivel de api y utilizando postman se realizan el siguiente crud para los amenities

* http://165.227.92.20/api/amenitie para guardar amenities por post
* http://165.227.92.20/api/amenities para consultar todos los amenities
* http://165.227.92.20/api/amenitie/:idAmenitie para consultar un amenitie en especifico
* http://165.227.92.20/api/amenitie/:idAmenitie para modificar un amenitie por patch
* http://165.227.92.20/api/amenitie/:idAmenitie para eliminar un amenitie por delete
* http://165.227.92.20/api/amenitie/image/:image para consultar la imagen de un amenitie

# A nivel de api y utilizando postman se realizan el siguiente crud para los hoteles

* http://165.227.92.20/api/hotel para guardar un hotel por post
* http://165.227.92.20/api/hotels para consultar los hoteles
* http://165.227.92.20/api/hotel/:idHotel para consultar un hotel en especifico
* http://165.227.92.20/api/hotel/:idHotel para modificar  un hotel en especifico por patch
* http://165.227.92.20/api/hotel/:idHotel para eliminar  un hotel en especifico por delete
* http://165.227.92.20/api/hotel/image/:image para consultar una imagen del hotel


LA DOCUMENTACION SE ENCUENTRA EN EL PROYECTO API

