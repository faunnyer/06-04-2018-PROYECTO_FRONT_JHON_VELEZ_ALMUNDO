import { Http, Headers } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';


@Injectable()
export class HotelesService {

   url = 'http://165.227.92.20/api/hotels';
  // url = 'http://localhost:3000/api/hotels';

  constructor(private http: Http) { }

  getHotels() {
    return this.http.get(this.url).map(res => res.json());
  }

}
