import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroHoteles'
})
export class FiltroHotelesPipe implements PipeTransform {

  // Pipe utilizado para el filtro de hoteles

  transform(items: any[], filter: string, starFilters: number[]): any {
    let array: any[] = items;
    if (starFilters.length <= 0) {
      starFilters = [1, 2, 3, 4, 5];
    }
    array = items.filter(item => {
        const okFilterName = filter && this.compareIndexOf(item, filter) || (filter === '');
        const okFilterStars = this.getStarNumberComparator(item, starFilters);
        return okFilterName && okFilterStars;
    });

    return array;
  }

  compareIndexOf(item: any, filter: string) {
    const filterName = filter && filter.toLowerCase();
    const itemName = item.name.toLowerCase();
    return itemName.indexOf(filterName) !== -1;
  }

  getStarNumberComparator(item: any, stars: number[]) {
    return stars.filter(star => item.stars === star).length > 0;
  }
}

