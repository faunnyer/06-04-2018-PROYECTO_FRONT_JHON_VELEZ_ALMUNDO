import { Component, OnInit } from '@angular/core';
import { HotelesService } from '../../servicios/hoteles.service';

import { Router } from '@angular/router';
import { error } from 'util';
import { FILTRO_ESTRELLAS } from '../model/stars.const';


@Component({
  selector: 'app-hoteles',
  templateUrl: './hoteles.component.html'
})
export class HotelesComponent implements OnInit {

  // Lista de hoteles
  hoteles: any[] = [];
  hotelesFiltrados = '';
  filtrosPorEstrella: number[] = [];
  filtroEstrellasConst = FILTRO_ESTRELLAS;

  // array para el ciclo
  Arr = Array;

  constructor(private _HotelesService: HotelesService,
    private router: Router
  ) {
  }

  ngOnInit() {
    // servicio que consulta todos los hoteles
    this._HotelesService.getHotels().subscribe(data => {
      this.hoteles = data.object;
    }, error => {
      console.log(error);
    });

  }

  traerTodo() {
    this.filtrosPorEstrella.length = 0;
    this._HotelesService.getHotels().subscribe(data => {
      this.hoteles = data.object;
    }, error => {
      console.log(error);
    });

  }

  modificarFiltrosXEstrellas(filtro) {
    filtro.seleccionado = !filtro.seleccionado;
    this.filtrosPorEstrella = this.filtroEstrellasConst.filter(miFiltro => miFiltro.seleccionado === true)
      .map(estrella => estrella.numero);
  }
}
