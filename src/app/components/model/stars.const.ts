export const FILTRO_ESTRELLAS = [
    { tipoHTML: 'checkbox', seleccionado: false, codigo: 'todas', numero: 0 },
    { tipoHTML: 'checkbox', seleccionado: false, codigo: 'una', numero: 1 },
    { tipoHTML: 'checkbox', seleccionado: false, codigo: 'dos', numero: 2 },
    { tipoHTML: 'checkbox', seleccionado: false, codigo: 'tres', numero: 3 },
    { tipoHTML: 'checkbox', seleccionado: false, codigo: 'cuatro', numero: 4 },
    { tipoHTML: 'checkbox', seleccionado: false, codigo: 'cinco', numero: 5 },
];
