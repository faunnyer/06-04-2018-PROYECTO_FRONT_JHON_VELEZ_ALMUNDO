
import { RouterModule, Routes } from '@angular/router';
import { HotelesComponent } from './components/hoteles/hoteles.component';

const APP_ROUTES: Routes = [
  { path: 'hoteles', component: HotelesComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'hoteles' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
