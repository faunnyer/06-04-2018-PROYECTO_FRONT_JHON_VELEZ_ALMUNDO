import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// Rutas
import { APP_ROUTING } from './app.routes';

// Servicios
import { HotelesService } from './servicios/hoteles.service';

// Componentes
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HotelesComponent } from './components/hoteles/hoteles.component';
import { FiltroHotelesPipe } from './pipes/filtro-hoteles.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HotelesComponent,
    FiltroHotelesPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    APP_ROUTING
  ],
  providers: [
    HotelesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
